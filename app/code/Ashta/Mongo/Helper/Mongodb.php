<?php 
namespace Ashta\Mongo\Helper;

use Magento\Store\Model\ScopeInterface;

class Mongodb 
        extends \Magento\Framework\App\Helper\AbstractHelper {

    /**
    * @var MongoDB Name
    */
    protected $dbname;  
    
    /**
    * @var MongoDB Host
    */
    protected $dbhost;  
    
    /**
    * @var MongoDB Port
    */
    protected $dbport;  
    
    /**
    * @var MongoDB Connection
    */
    protected $connection;
    
    /**
    * @var \Magento\Framework\App\Config\ScopeConfigInterface
    */
    protected $scopeConfig;
        
    const STORE_ID = 0;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig) {
        $this->scopeConfig = $scopeConfig;
        $this->dbhost = $this->scopeConfig->getValue(
                        'mongo/general/dbhost', 
                        ScopeInterface::SCOPE_STORE,
                        self::STORE_ID);
        $this->dbname = $this->scopeConfig->getValue(
                        'mongo/general/dbname', 
                        ScopeInterface::SCOPE_STORE,
                        self::STORE_ID);
        $this->dbport = $this->scopeConfig->getValue(
                        'mongo/general/dbport', 
                        ScopeInterface::SCOPE_STORE,
                        self::STORE_ID);
        try {
            //Establish database connection
            $this->connection = new \MongoDB\Driver\Manager('mongodb://'.$this->dbhost.':'.$this->dbport);
        }catch (MongoDBDriverExceptionException $e) {
            echo $e->getMessage();
            echo nl2br("n");
        }
    }

    /* 
    * Function to connect Mongo DB server using constructor init
    */
    public function getConnection() {
        return $this->connection;
    }

    /* 
    * Get Mongo DB name
    */
    public function getDBname() {
        return $this->dbname;
    }

    /**
     * Get website id by website code
     * 
     * @param string $websiteCode
     * @return int
     */
    public function getWebsiteId($websiteCode) {
        $websiteId = self::DEFAULT_WEBSITE_ID;
        $collection = $this->websiteCollectionFactory->create();
        $websiteCollection = $collection->addFieldToFilter('code', $websiteCode);
        if (!empty($websiteCollection->getFirstItem()->getData())) {
            $websiteId = $websiteCollection->getFirstItem()->getWebsiteId();
        }

        return $websiteId;
    }

    /*
    * Insert records into Mongo DB tables
    */
    public function insert($collection, $data, $indexer) {
        $response = null;
        if (!empty($data['sku'])) {
            $sku = $data['sku'];
            $magentoProduct = array("id" => $indexer,
                                "name" => $data['product_name'],
                                "sku" => $sku,
                                "price" => $data['price'],
                                "qty" => $data['qty']
                                );
            $response = $this->createMagentoProduct($magentoProduct);
            if ($response == 'success') {
                $inserts = new \MongoDB\Driver\BulkWrite();
                $inserts->insert($data);
                $this->getConnection()->executeBulkWrite("{$this->dbname}.{$collection}", $inserts);
                echo "{$sku} - inserted succesfully...\n";
            } else {
                echo "Something went wrong for SKU:{$sku}";
            }
        }
    }

    public function getData($collection, $filters = array(), $options = array()) {
        if (!is_array($filters)) {
            return false;
        }
        $option = [];
        $read = new \MongoDB\Driver\Query($filters, $option);
        $single_user = $this->getConnection()->executeQuery("{$this->dbname}.{$collection}", $read);
        foreach ($single_user as $user) {
            return $user;
        }
    }

    public function createMagentoProduct($data) {
        $sku = $data['sku'];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('\Magento\Catalog\Model\Product');
        $product->setSku($sku);
        $product->setName($data['name']);
        $product->setTypeId('simple');
        $product->setStatus(1);
        $product->setAttributeSetId(4);
        $product->setWebsiteIds(array(1));
        $product->setVisibility(4);
        $product->setPrice(array($data['price']));
        $product->setStockData(array(
                'use_config_manage_stock' => 0,
                'manage_stock' => 1,
                'is_in_stock' => 1,
                'qty' => $data['qty']
                )
            );
        try{
            $product->save();
            $return = "success";
            echo "Product created inside Magento...{$sku}\n";
        } catch (Exception $ex) {
            echo "Error for SKU:{$sku} - ".$ex->getMessage()."\n";
            return "falied";
        }
        return $return;
    }
}