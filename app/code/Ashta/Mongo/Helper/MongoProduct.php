<?php 
namespace Ashta\Mongo\Helper;

use Magento\Store\Model\ScopeInterface;

class MongoProduct 
        extends \Magento\Framework\App\Helper\AbstractHelper {

    public function getProductDataBySku($sku) {
        if (empty($sku)) {
            return false;
        }
        $product = array (
            "product_name" => "The Cat Returns: Neko No Ongaeshi",
            "sku" => "0786936268836",
            "price" => "000000",
            "qty" => "999",
            "attributes" => array (
                "awards" => "",
                "author_information" => "",
                "annotations" => "",
                "author_biography" => "",
                "format" => "Video",
                "jacket_description" => "",
                "music_details" => "TEST",
                "titles_and_details" => "The Cat Returns: Neko No Ongaeshi",
                "format_of_books" => "TEST",
                "book_format_code" => "DVD-Audio",                     
                "bisac_subject_code_minor" => "",
                "bisac_subject_code_major" => "Biography & Autobiography",
                "language" => "TEST",
                "media" => "TEST",
                "isbn_10" => "5559296422",
                "isbn_13" => "0786936268836",
                "publication_date" => "",
                "discounts" => "TEST",
                "target_age_group" => "min:08, max:12",
                "product_description" => "TEST",
                "series" => "American Experience (PBS Home Video)",
                "physical_info" => "TEST",
                "grade_level" => "TEST"
            )
        );
        return $product;  
    }
}